var app = angular.module("contact", []);

app.controller("ContactCtrl", ["$scope", "$http", "$q", '$timeout',
    function($scope, $http, $q, $timeout){
        $scope.form = {};
        var url = "http://127.0.0.1:8000/api/contact/";

        var get = function(){
            function onSuccess(response){
                $scope.contacts = response.data;
            }

            function onError(errors){
                console.log(errors);
            }

            $http.get(url).then(onSuccess).catch(onError);
        };

        // add contact 
        $scope.addContact = function(){
            var data = $scope.form;
            
            function onSuccess(response, status, hearder, config){
                $scope.form ={};
                // re-loading the contact list
                get()
                $scope.success = true;
                $scope.failed = false;

                $timeout(function(){
                    $scope.success = false;
                }, 2000);
            }

            function onError(error, status, hearder, config){
                console.log(error);
                $scope.failed = true;
                $scope.success = false;

                $timeout(function(){
                    $scope.failed = false;
                }, 2000);
            }


            $http.post(url, data).then(onSuccess).catch(onError);

        };


        // loading the list 
        get();
    }
]);